## To Do

- Add 'edit' feature
- Create pass(1) with common information  and syntax description
- Create pass(5) with structure of `.password-store` folder

## Doing


## Done

- Add account existence check
- Change PKGBUILD's install method
- Move TODO to TODO.md
- Change arguments handling method
- Add 'remove' feature
